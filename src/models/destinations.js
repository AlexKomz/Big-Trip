export default class Destinations {
  constructor() {
    this._destinations = [];
  }

  get Destinations() {
    return this._destinations;
  }

  set Destinations(destinations) {
    this._destinations = Array.from(destinations).map((destination) => {
      return {
        name: destination.name,
        description: destination.description,
        pictures: Array.from(destination.pictures)
      };
    });
  }

  get Cities() {
    return this._destinations.map((destination) => destination.name);
  }

  getDetails(name) {
    const destinations = this._destinations.find((destination) => destination.name === name);
    return destinations ? {
      description: destinations.description,
      pictures: Array.from(destinations.pictures)
    } : {
      description: ``,
      pictures: []
    };
  }
}
