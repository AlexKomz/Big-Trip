export default class Offers {
  constructor() {
    this._offers = [];
  }

  get Offers() {
    return this._offers;
  }

  set Offers(offers) {
    this._offers = Array.from(offers).map((offer) => {
      return {
        type: offer.type,
        offers: Array.from(offer.offers),
      };
    });
  }

  getOffers(type) {
    const offers = this._offers.find((offer) => offer.type === type.toLowerCase());
    return offers ? offers.offers : [];
  }
}
