import {getDateDuration} from "../utils/common";

export default class Point {
  constructor(data) {
    this.id = data[`id`];
    this.type = data[`type`];
    this.destination = data[`destination`];
    this.beginDate = new Date(data[`date_from`]);
    this.endDate = new Date(data[`date_to`]);
    this.duration = getDateDuration(this.beginDate, this.endDate);
    this.basePrice = Number(data[`base_price`]);
    this.isFavorite = Boolean(data[`is_favorite`]);
    this.offers = Array.from(data[`offers`]);
  }

  toRAW() {
    return {
      "id": this.id,
      "type": this.type,
      "date_from": this.beginDate.toISOString(),
      "date_to": this.endDate.toISOString(),
      "destination": this.destination,
      "base_price": this.basePrice,
      "is_favorite": this.isFavorite,
      "offers": this.offers
    };
  }

  static parsePoint(data) {
    return new Point(data);
  }

  static parsePoints(data) {
    return data.map(Point.parsePoint);
  }

  static clone(data) {
    return new Point(data.toRAW());
  }
}
