const durationCompare = (pointA, pointB) => {
  const minutesA = pointA.duration.days * 24 * 60 + pointA.duration.hours * 60 + pointA.duration.minutes;
  const minutesB = pointB.duration.days * 24 * 60 + pointB.duration.hours * 60 + pointB.duration.minutes;

  return minutesA - minutesB;
};

export const sortPointUpByTime = (pointA, pointB) => {
  return durationCompare(pointA, pointB);
};

export const sortPointDownByTime = (pointA, pointB) => {
  return durationCompare(pointB, pointA);
};

export const sortPointUpByPrice = (pointA, pointB) => pointA.basePrice - pointB.basePrice;

export const sortPointDownByPrice = (pointA, pointB) => pointB.basePrice - pointA.basePrice;
