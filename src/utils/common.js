import moment from "moment";


export const hoursMinutsFromDate = (date) => {
  return moment(date).format(`HH:MM`);
};

export const getDatetimeString = (date, isTime = false) => {
  return isTime
    ? moment(date).format()
    : moment(date).format();
};

export const sliceMonthName = (date) => {
  return moment(date).format(`MMM`);
};

export const isFutureDate = (date) => {
  const now = moment(new Date());
  const comparableDate = moment(date);

  return comparableDate.isAfter(now, `days`);
};

export const isPastDate = (date) => {
  const now = moment(new Date());
  const comparableDate = moment(date);

  return now.isAfter(comparableDate, `days`);
};

export const getDateFromString = (date) => {
  const parsedDate = date.split(`/`);

  return new Date(`${parsedDate[1]}/${parsedDate[0]}/${parsedDate[2]}`);
};

export const getDateDuration = (begin, end) => {
  let minutes = Math.floor((end - begin) / 60000);
  let hours = Math.floor(minutes / 60);
  minutes = minutes - hours * 60;
  let days = Math.floor(hours / 24);
  hours = hours - days * 24;

  return {days, hours, minutes};
};

export const dateComparison = (lastDate, newDate) => {
  return (
    newDate.getDate() > lastDate.getDate() ||
    newDate.getMonth() > lastDate.getMonth() ||
    newDate.getFullYear() > lastDate.getFullYear()
  );
};
