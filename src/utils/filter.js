import {FilterType} from "../consts";
import {isFutureDate, isPastDate} from "./common";

export const getFuturePoints = (points) => {
  return points.filter((point) => isFutureDate(point.beginDate));
};

export const getPastPoints = (points) => {
  return points.filter((point) => isPastDate(point.beginDate));
};

export const getPointsByFilter = (points, filterType) => {
  switch (filterType) {
    case FilterType.FUTURE:
      return getFuturePoints(points);
    case FilterType.PAST:
      return getPastPoints(points);
  }

  return points;
};
