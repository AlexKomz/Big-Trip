import SiteInfoMenuComponent from "../components/site-info-menu";
import {sliceMonthName} from "../utils/common";
import {render, replace, RenderPosition} from "../utils/render";

export default class SiteInfoMenuController {
  constructor(container, pointsModel) {
    this._container = container;
    this._pointsModel = pointsModel;

    this._siteInfoData = {};

    this._siteInfoMenuComponent = null;

    this._onDataChange = this._onDataChange.bind(this);

    this._pointsModel.setDataChangeHandler(this._onDataChange);
    this._pointsModel.setFilterChangeHandler(this._onDataChange);
  }

  render() {
    const container = this._container;
    const oldComponent = this._siteInfoMenuComponent;

    this._initData();

    this._siteInfoMenuComponent = new SiteInfoMenuComponent(this._siteInfoData);

    if (oldComponent) {
      replace(this._siteInfoMenuComponent, oldComponent);
    } else {
      render(container, this._siteInfoMenuComponent, RenderPosition.AFTERBEGIN);
    }
  }

  _onDataChange() {
    this.render();
  }

  _initData() {
    const sortedPoints = this._pointsModel.Points.sort((left, right) => left.beginDate - right.beginDate);

    if (sortedPoints.length !== 0) {
      const cost = sortedPoints.reduce((acc, current) => acc + current.basePrice, 0);
      const firstPoint = sortedPoints[0];
      const lastPoint = sortedPoints[sortedPoints.length - 1];

      let dateFrom = ``;
      let dateTo = ``;

      const cities = new Set();

      const firstMounth = sliceMonthName(firstPoint.beginDate);
      const lastMounth = sliceMonthName(lastPoint.endDate);

      dateFrom = `${firstMounth} ${firstPoint.beginDate.getDate()}`;
      dateTo = `${firstMounth === lastMounth ? `` : lastMounth + ``}${lastPoint.endDate.getDate()}`;

      sortedPoints.forEach((point) => cities.add(point.destination.name));

      this._siteInfoData = {
        cost,
        dateFrom,
        dateTo,
        cities: Array.from(cities)
      };
    } else {
      this._siteInfoData = null;
    }
  }
}
