import PointComponent from "../components/point";
import PointAddComponents from "../components/point-add";
import PointEditComponent from "../components/point-edit";
import PointModel from "../models/point";
import {render, replace, remove, isEmpty, RenderPosition} from "../utils/render";
import {getDateFromString} from "../utils/common";


const SHAKE_ANIMATION_TIMEOUT = 600;

export const Mode = {
  ADDING: `adding`,
  DEFAULT: `default`,
  EDIT: `edit`,
};

const parseFormData = (data, formData) => {
  const bDate = formData.get(`event-start-time`);
  const eDate = formData.get(`event-end-time`);
  const beginDate = getDateFromString(bDate);
  const endDate = getDateFromString(eDate);

  return new PointModel(Object.assign({}, data, {
    "base_price": formData.get(`event-price`),
    "date_from": beginDate,
    "date_to": endDate,
  }));
};

export const EmptyPoint = {
  type: ``,
  destination: {
    name: ``,
    description: ``,
    pictures: []
  },
  beginDate: new Date(),
  endDate: new Date(),
  duration: null,
  basePrice: 0,
  isFavorite: false,
  offers: []
};

export default class PointController {
  constructor(container, day, onDataChange, onViewChange) {
    this._container = container;
    this._day = day;
    this._onDataChange = onDataChange;
    this._onViewChange = onViewChange;

    this._mode = Mode.DEFAULT;

    this._pointComponent = null;
    this._pointEditComponent = null;
    this._pointAddComponent = null;

    this._onEscKeyDown = this._onEscKeyDown.bind(this);
  }

  render(point, mode, destinationsModel, offersModel, button = null) {
    const oldPointComponent = this._pointComponent;
    const oldPointEditComponent = this._pointEditComponent;
    const oldPointAddComponent = this._pointAddComponent;

    this._mode = mode;

    if (mode === Mode.ADDING) {
      this._pointAddComponent = new PointAddComponents(point, destinationsModel, offersModel);

      this._pointAddComponent.setFormSubmitHandler((evt) => {
        evt.preventDefault();

        const data = this._pointAddComponent.Data;
        const formData = this._pointAddComponent.formData;
        const newData = parseFormData(data, formData);

        this._pointAddComponent.Data = {
          saveButtonText: `Saving...`,
        };

        button.disabled = false;
        this._onDataChange(this, EmptyPoint, newData);
      });

      this._pointAddComponent.setCloseClickHandler(() => {
        button.disabled = false;
        this._onDataChange(this, null, null);
      });
    } else {
      this._pointComponent = new PointComponent(point);
      this._pointEditComponent = new PointEditComponent(point, destinationsModel, offersModel);

      this._pointComponent.setEditClickHandler(() => {
        this._replacePointToEdit();
        document.addEventListener(`keydown`, this._onEscKeyDown);
      });

      this._pointEditComponent.setFormSubmitHandler((evt) => {
        evt.preventDefault();

        const data = this._pointEditComponent.Data;
        const formData = this._pointEditComponent.formData;
        const newData = parseFormData(data, formData);

        this._pointEditComponent.Data = {
          saveButtonText: `Saving...`,
        };

        this._onDataChange(this, point, newData);
      });

      this._pointEditComponent.setDeleteButtonClickHandler(() => {
        this._pointEditComponent.Data = {
          deleteButtonText: `Deleting...`,
        };

        this._onDataChange(this, point, null);
      });

      this._pointEditComponent.setCloseClickHandler(() => {
        this._replaceEditToPoint();
      });
    }

    switch (mode) {
      case Mode.DEFAULT:
        if (oldPointComponent && oldPointEditComponent) {
          replace(this._pointComponent, oldPointComponent);
          replace(this._pointEditComponent, oldPointEditComponent);
          this._replaceEditToPoint();
        } else {
          render(this._container, this._pointComponent, RenderPosition.BEFOREEND);
        }
        break;
      case Mode.ADDING:
        if (oldPointAddComponent && oldPointComponent) {
          remove(oldPointComponent);
          remove(oldPointAddComponent);
        }
        render(this._container, this._pointAddComponent, RenderPosition.BEFOREBEGIN);
        break;
    }
  }

  setDefaultView() {
    if (this._mode !== Mode.DEFAULT) {
      this._replaceEditToPoint();
    }
  }

  destroy() {
    if (this._pointEditComponent) {
      remove(this._pointEditComponent);
    }

    if (this._pointComponent) {
      remove(this._pointComponent);
    }

    if (this._pointAddComponent) {
      remove(this._pointAddComponent);
    }

    document.removeEventListener(`keydown`, this._onEscKeyDown);

    if (isEmpty(this._container)) {
      remove(this._day);
    }
  }

  shake() {
    if (this._pointAddComponent) {
      this._pointAddComponent.getElement().style.animation = `shake ${SHAKE_ANIMATION_TIMEOUT / 1000}s`;
    }
    if (this._pointEditComponent) {
      this._pointEditComponent.getElement().style.animation = `shake ${SHAKE_ANIMATION_TIMEOUT / 1000}s`;
    }
    if (this._pointComponent) {
      this._pointComponent.getElement().style.animation = `shake ${SHAKE_ANIMATION_TIMEOUT / 1000}s`;
    }

    setTimeout(() => {
      if (this._pointAddComponent) {
        this._pointAddComponent.getElement().style.animation = ``;

        this._pointAddComponent.Data = {
          saveButtonText: `Save`,
        };
      }
      if (this._pointEditComponent) {
        this._pointEditComponent.getElement().style.animation = ``;

        this._pointEditComponent.Data = {
          saveButtonText: `Save`,
          deleteButtonText: `Delete`,
        };
      }
      if (this._pointComponent) {
        this._pointComponent.getElement().style.animation = ``;
      }
    }, SHAKE_ANIMATION_TIMEOUT);
  }

  _replaceEditToPoint() {
    document.removeEventListener(`keydown`, this._onEscKeyDown);
    this._pointEditComponent.reset();

    if (document.contains(this._pointEditComponent.getElement())) {
      replace(this._pointComponent, this._pointEditComponent);
    }

    this._mode = Mode.DEFAULT;
  }

  _replacePointToEdit() {
    this._onViewChange();
    replace(this._pointEditComponent, this._pointComponent);
    this._mode = Mode.EDIT;
  }

  _onEscKeyDown(evt) {
    const isEscKey = evt.key === `Escape` || evt.key === `Esc`;

    if (isEscKey) {
      this._replaceEditToPoint();
      document.removeEventListener(`keydown`, this._onEscKeyDown);
    }
  }
}
