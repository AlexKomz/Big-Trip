import NoPointComponent from "../components/no-point";
import SortComponent from "../components/sort";
import DaysListComponent from "../components/days-list";
import DayComponent from "../components/day";
import DayInfoComponent from "../components/day-info";
import TripListComponent from "../components/trip-list";

import {HIDDEN_CLASS} from "../components/abstract-component";

import PointController, {Mode as PointControllerMode, EmptyPoint} from "./point";

import {render, RenderPosition} from "../utils/render";
import {sortPointUpByTime, sortPointDownByTime, sortPointUpByPrice, sortPointDownByPrice} from "../utils/point";
import {dateComparison} from "../utils/common";
import {SortType} from "../consts";


const renderDay = (daysListComponent, dayComponent, mode, daysCount = ``, point = ``) => {
  const dayInfoComponent = new DayInfoComponent(daysCount, point);

  const tripListComponent = new TripListComponent();

  if (mode === PointControllerMode.DEFAULT) {
    render(daysListComponent, dayComponent, RenderPosition.BEFOREEND);
  } else {
    render(daysListComponent, dayComponent, RenderPosition.AFTERBEGIN);
  }
  render(dayComponent, dayInfoComponent, RenderPosition.BEFOREEND);
  render(dayComponent, tripListComponent, RenderPosition.BEFOREEND);

  return tripListComponent;
};

const getPointController = (tripListComponent, dayComponent, point, destinationsModel, offersModel, onDataChange, onViewChange, mode = PointControllerMode.DEFAULT) => {
  const pointController = new PointController(tripListComponent, dayComponent, onDataChange, onViewChange);

  pointController.render(point, mode, destinationsModel, offersModel);

  return pointController;
};

const renderPoints = (daysListComponent, points, sortType, destinationsModel, offersModel, onDataChange, onViewChange) => {
  let tripListComponent = null;
  let dayComponent = null;

  if (sortType === SortType.DEFAULT) {
    let daysCount = 1;
    let lastDate = null;

    return points.map((point) => {
      if (!lastDate || dateComparison(lastDate, point.beginDate)) {
        lastDate = point.beginDate;

        dayComponent = new DayComponent();

        tripListComponent = renderDay(daysListComponent, dayComponent, PointControllerMode.DEFAULT, daysCount, point);

        daysCount++;

        return getPointController(tripListComponent, dayComponent, point, destinationsModel, offersModel, onDataChange, onViewChange);
      } else {
        return getPointController(tripListComponent, dayComponent, point, destinationsModel, offersModel, onDataChange, onViewChange);
      }
    });
  } else {
    dayComponent = new DayComponent();

    tripListComponent = renderDay(daysListComponent, dayComponent, PointControllerMode.DEFAULT);

    return points.map((point) => {
      return getPointController(tripListComponent, dayComponent, point, destinationsModel, offersModel, onDataChange, onViewChange);
    });
  }
};

const getSortedPoints = (points, sortType, from, to) => {
  let sortedPoints = [];
  const showingPoints = points.slice();

  switch (sortType) {
    case SortType.TIME_UP:
      sortedPoints = showingPoints.sort(sortPointUpByTime);
      break;
    case SortType.TIME_DOWN:
      sortedPoints = showingPoints.sort(sortPointDownByTime);
      break;
    case SortType.PRICE_UP:
      sortedPoints = showingPoints.sort(sortPointUpByPrice);
      break;
    case SortType.PRICE_DOWN:
      sortedPoints = showingPoints.sort(sortPointDownByPrice);
      break;
    default:
      sortedPoints = showingPoints.sort((pointA, pointB) => pointA.beginDate - pointB.beginDate);
  }

  return sortedPoints.slice(from, to);
};

export default class TripController {
  constructor(container, destinationsModel, offersModel, pointsModel, api) {
    this._container = container;
    this._destinationsModel = destinationsModel;
    this._offersModel = offersModel;
    this._pointsModel = pointsModel;
    this._api = api;

    this._showedPointControllers = [];
    this._noPointComponent = new NoPointComponent();
    this._sortComponent = new SortComponent();
    this._daysListComponent = new DaysListComponent();
    this._creatingPoint = null;

    this._onDataChange = this._onDataChange.bind(this);
    this._onSortTypeChange = this._onSortTypeChange.bind(this);
    this._onViewChange = this._onViewChange.bind(this);
    this._onFilterChange = this._onFilterChange.bind(this);
    this._onPointsDataChange = this._onPointsDataChange.bind(this);

    this._sortComponent.setSortTypeChangeHandler(this._onSortTypeChange);
    this._pointsModel.setFilterChangeHandler(this._onFilterChange);
    this._pointsModel.setDataChangeHandler(this._onPointsDataChange);
  }

  hide() {
    if (this._container) {
      this._container.classList.add(HIDDEN_CLASS);
    }
    this._sortComponent.setSortType(SortType.DEFAULT);
  }

  show() {
    if (this._container) {
      this._container.classList.remove(HIDDEN_CLASS);
    }
    this._updatePoints();
  }

  render() {
    this._updatePoints();
  }

  createPoint(createButton) {
    if (this._creatingPoint) {
      return;
    }

    this._switchingContent(true);

    createButton.disabled = true;

    const dayComponent = new DayComponent();
    const tripListComponent = renderDay(this._daysListComponent, dayComponent, PointControllerMode.ADDING);
    const pointController = new PointController(tripListComponent, dayComponent, this._onDataChange, this._onViewChange);

    pointController.render(EmptyPoint, PointControllerMode.ADDING, this._destinationsModel, this._offersModel, createButton);
  }

  _removePoints() {
    this._showedPointControllers.forEach((pointController) => pointController.destroy());
    this._showedPointControllers = [];
  }

  _renderPoints(points) {
    const sortType = this._sortComponent.getSortType();

    this._showedPointControllers = renderPoints(this._daysListComponent, points, sortType, this._destinationsModel, this._offersModel, this._onDataChange, this._onViewChange);
  }

  _updatePoints() {
    this._switchingContent();

    const sortedPoints = getSortedPoints(this._pointsModel.Points, this._sortComponent.getSortType(), 0, this._pointsModel.PointsCount);

    this._removePoints();
    this._renderPoints(sortedPoints);
  }

  _onDataChange(pointController, oldData, newData) {
    let isSuccess = false;

    if (oldData === null && newData === null) {
      this._creatingPoint = null;

      pointController.destroy();

      this._updatePoints();
    } else if (oldData === EmptyPoint) {
      this._creatingPoint = null;
      if (newData !== null) {
        this._api.createPoint(newData)
          .then((pointModel) => {
            pointController.destroy();

            this._pointsModel.addPoint(pointModel);

            this._updatePoints();
          })
          .catch(() => {
            pointController.shake();
          });
      }
    } else if (newData === null) {
      this._api.deletePoint(oldData.id)
        .then(() => {
          isSuccess = this._pointsModel.removePoint(oldData.id);

          if (isSuccess) {
            this._updatePoints();
          }
        })
        .catch(() => {
          pointController.shake();
        });
    } else {
      this._api.updatePoint(oldData.id, newData)
        .then((pointModel) => {
          isSuccess = this._pointsModel.updatePoint(oldData.id, pointModel);

          if (isSuccess) {
            this._updatePoints();
          }
        })
        .catch(() => {
          pointController.shake();
        });
    }
  }

  _onViewChange() {
    this._showedPointControllers.forEach((it) => it.setDefaultView());
  }

  _onSortTypeChange(sortType) {
    const sortedPoints = getSortedPoints(this._pointsModel.Points, sortType, 0, this._pointsModel.PointsCount);

    this._removePoints();
    this._renderPoints(sortedPoints);
  }

  _onFilterChange() {
    this._updatePoints();
  }

  _switchingContent(adding = false) {
    const points = this._pointsModel.Points;

    if (points.length === 0 && !adding) {
      this._sortComponent.getElement().remove();
      this._daysListComponent.getElement().remove();

      if (!this._container.contains(this._noPointComponent.getElement())) {
        render(this._container, this._noPointComponent, RenderPosition.BEFOREEND);
      }
    } else {
      this._noPointComponent.getElement().remove();

      if (!this._container.contains(this._sortComponent.getElement())) {
        render(this._container, this._sortComponent, RenderPosition.BEFOREEND);
      }

      if (!this._container.contains(this._daysListComponent.getElement())) {
        render(this._container, this._daysListComponent, RenderPosition.BEFOREEND);
      }
    }
  }

  _onPointsDataChange() {
    this._switchingContent();
  }
}
