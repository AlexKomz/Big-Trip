import Point from "../models/point";
import {nanoid} from "nanoid";

const isOnline = () => {
  return window.navigator.onLine;
};

const getSyncedPoints = (items) => {
  return items.filter(({success}) => success)
    .map(({payload}) => payload.point);
};

const creteStoreStructure = (items) => {
  return items.reduce((acc, current) => {
    return Object.assign({}, acc, {
      [current.id]: current
    });
  }, {});
};

export default class Provider {
  constructor(api, stores) {
    this._api = api;
    this._stores = stores;
  }

  get Destinations() {
    if (isOnline()) {
      return this._api.Destinations
        .then((destinations) => {
          const items = creteStoreStructure(destinations);

          this._stores.destinations.setItems(items);

          return destinations;
        });
    }

    const storeDestinations = Object.values(this._stores.destinations.getItems());

    return Promise.resolve(storeDestinations);
  }

  get Offers() {
    if (isOnline()) {
      return this._api.Offers
        .then((offers) => {
          const items = creteStoreStructure(offers);

          this._stores.offers.setItems(items);

          return offers;
        });
    }

    const storeOffers = Object.values(this._stores.offers.getItems());

    return Promise.resolve(storeOffers);
  }

  get Points() {
    if (isOnline()) {
      return this._api.Points
        .then((points) => {
          const items = creteStoreStructure(points.map((point) => point.toRAW()));

          this._stores.points.setItems(items);

          return points;
        });
    }

    const storePoints = Object.values(this._stores.points.getItems());

    return Promise.resolve(Point.parsePoints(storePoints));
  }

  createPoint(point) {
    if (isOnline()) {
      return this._api.createPoint(point)
        .then((newPoint) => {
          this._stores.points.setItem(newPoint.id, newPoint.toRAW());

          return newPoint;
        });
    }

    const localNewPointId = nanoid();
    const localNewPoint = Point.clone(Object.assign(point, {id: localNewPointId}));

    this._stores.points.setItem(localNewPoint.id, localNewPoint.toRAW());

    return Promise.resolve(localNewPoint);
  }

  updatePoint(id, point) {
    if (isOnline()) {
      return this._api.updatePoint(id, point)
        .then((newPoint) => {
          this._stores.points.setItem(newPoint.id, newPoint.toRAW());

          return newPoint;
        });
    }

    const localPoint = Point.clone(Object.assign(point, {id}));

    this._stores.points.setItem(id, localPoint.toRAW());

    return Promise.resolve(localPoint);
  }

  deletePoint(id) {
    if (isOnline()) {
      return this._api.deletePoint(id)
        .then(() => this._stores.points.removeItem(id));
    }

    this._stores.points.removeItem(id);

    return Promise.resolve();
  }

  sync() {
    if (isOnline()) {
      const storePoints = Object.values(this._stores.points.getItems());

      return this._api.sync(storePoints)
        .then((responce) => {
          const createdPoints = getSyncedPoints(responce.created);
          const updatedPoints = getSyncedPoints(responce.updated);

          const items = creteStoreStructure([...createdPoints, ...updatedPoints]);

          this._stores.points.setItems(items);
        });
    }

    return Promise.reject(new Error(`Sync data failed`));
  }
}
