export const SortType = {
  DEFAULT: `default`,
  TIME_DOWN: `time-down`,
  TIME_UP: `time-up`,
  PRICE_DOWN: `price-down`,
  PRICE_UP: `price-up`
};

export const FilterType = {
  EVERYTHING: `everything`,
  FUTURE: `future`,
  PAST: `past`
};
