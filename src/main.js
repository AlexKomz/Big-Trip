import API from "./api/index";
import Store from "./api/store";
import Provider from "./api/provider";
import StatisticComponent from "./components/statistics";
import ControlTabsComponent, {MenuItem} from "./components/control-tabs";
import LoadingComponent from "./components/loading";
import TripController from "./controllers/trip";
import FilterController from "./controllers/filter";
import SiteInfoMenuController from "./controllers/site-info-menu";
import DestinationsModel from "./models/destinations";
import OffersModel from "./models/offers";
import PointsModel from "./models/points";
import {render, RenderPosition} from "./utils/render";


const AUTHORIZATION = `Basic dsaffafsfaf33424228`;
const END_POINT = `https://11.ecmascript.pages.academy/big-trip`;
const STORE_PREFIX = `big-trip-localstorage`;
const STORE_VER = `v1`;
const storeType = {
  DESTINATIONS: `destinations`,
  OFFERS: `offers`,
  POINTS: `points`
};

const api = new API(END_POINT, AUTHORIZATION);
const stores = {
  destinations: new Store(`${STORE_PREFIX}-${storeType.DESTINATIONS}-${STORE_VER}`, window.localStorage),
  offers: new Store(`${STORE_PREFIX}-${storeType.OFFERS}-${STORE_VER}`, window.localStorage),
  points: new Store(`${STORE_PREFIX}-${storeType.POINTS}-${STORE_VER}`, window.localStorage)
};
const apiWithProvider = new Provider(api, stores);

const tripMain = document.querySelector(`.trip-main`);
const tripControls = document.querySelector(`.trip-controls`);
const tripContent = document.querySelector(`.trip-events`);

const addPointButton = tripMain.querySelector(`.trip-main__event-add-btn`);

const destinationsModel = new DestinationsModel();
const offersModel = new OffersModel();
const pointsModel = new PointsModel();

const controlTabsComponent = new ControlTabsComponent();
const statisticComponent = new StatisticComponent(pointsModel);
const loadingComponent = new LoadingComponent();

const filterController = new FilterController(tripControls, pointsModel);
const tripController = new TripController(tripContent, destinationsModel, offersModel, pointsModel, apiWithProvider);
const siteInfoMenuController = new SiteInfoMenuController(tripMain, pointsModel);

const loading = Promise.resolve();

siteInfoMenuController.render();
render(tripControls.firstElementChild, controlTabsComponent, RenderPosition.AFTEREND);
filterController.render();
render(tripContent, statisticComponent, RenderPosition.AFTEREND);
statisticComponent.hide();
render(tripContent, loadingComponent, RenderPosition.BEFOREEND);

controlTabsComponent.setOnChange((menuItem) => {
  switch (menuItem) {
    case MenuItem.TABLE:
      statisticComponent.hide();
      tripController.show();
      break;
    case MenuItem.STATS:
      tripController.hide();
      statisticComponent.show();
      break;
  }
});

addPointButton.addEventListener(`click`, (evt) => {
  controlTabsComponent.setDefaultActive();
  statisticComponent.hide();
  tripController.show();
  tripController.createPoint(evt.target);
});

loading
  .then(() => {
    apiWithProvider.Destinations
      .then((destinatins) => {
        destinationsModel.Destinations = destinatins;
      });
  })
  .then(() => {
    apiWithProvider.Offers
      .then((offers) => {
        offersModel.Offers = offers;
      });
  })
  .then(() => {
    apiWithProvider.Points
      .then((points) => {
        pointsModel.Points = points;
        loadingComponent.getElement().remove();
        tripController.render();
      });
  });

window.addEventListener(`load`, () => {
  navigator.serviceWorker.register(`/sw.js`)
    .then(() => {
      // Действие, в случае успешной регистрации ServiceWorker
    }).catch(() => {
      // Действие, в случае ошибки при регистрации ServiceWorker
    });
});

window.addEventListener(`online`, () => {
  document.title = document.title.replace(` [offline]`, ``);

  apiWithProvider.sync();
});

window.addEventListener(`offline`, () => {
  document.title += ` [offline]`;
});
