import AbstractComponent from "./abstract-component";

const createDayTemplate = () => {
  return (
    `<li class="trip-days__item  day"></li>`
  );
};

export default class Day extends AbstractComponent {
  getTemplate() {
    return createDayTemplate();
  }
}
