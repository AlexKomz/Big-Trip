import AbstractComponent from "./abstract-component";
import {getDatetimeString, sliceMonthName} from "../utils/common";

export const createDayInfoTemplate = (day, point) => {
  return (
    `<div class="day__info">
      <span class="day__counter">${day}</span>
      <time class="day__date" datetime="${point ? getDatetimeString(point.beginDate) : point}">${point ? sliceMonthName(point.beginDate) + ` ` + point.beginDate.getDate() : point}</time>
    </div>`
  );
};

export default class DayInfo extends AbstractComponent {
  constructor(day, point) {
    super();

    this._day = day;
    this._point = point;
  }

  getTemplate() {
    return createDayInfoTemplate(this._day, this._point);
  }
}

