import AbstractComponent from "./abstract-component";


export const MenuItem = {
  TABLE: `Table`,
  STATS: `Stats`
};

const ACTIVE_CLASS = `trip-tabs__btn--active`;

const createControlTabsTemplate = () => {
  return (
    `<nav class="trip-controls__trip-tabs  trip-tabs">
      <a class="trip-tabs__btn  trip-tabs__btn--active" href="#">Table</a>
      <a class="trip-tabs__btn" href="#">Stats</a>
    </nav>`
  );
};

export default class ControlTabs extends AbstractComponent {
  constructor() {
    super();
  }

  getTemplate() {
    return createControlTabsTemplate();
  }

  setDefaultActive() {
    Array.from(this.getElement().children).forEach((it) => it.classList.remove(ACTIVE_CLASS));
    Array.from(this.getElement().children)[0].classList.add(ACTIVE_CLASS);
  }

  setOnChange(handler) {
    this.getElement().addEventListener(`click`, (evt) => {
      if (evt.target.tagName !== `A`) {
        return;
      }

      Array.from(this.getElement().children).forEach((it) => it.classList.remove(ACTIVE_CLASS));

      const menuItem = evt.target.innerText;

      evt.target.classList.add(ACTIVE_CLASS);

      handler(menuItem);
    });
  }
}
