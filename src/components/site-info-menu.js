import AbstractComponent from "./abstract-component";

const createCitiesTemplate = (cities) => {
  return `<h1 class="trip-info__title">${cities.join(` &mdash; `)}</h1>`;
};

const createDateTemplate = (from, to) => {
  return `<p class="trip-info__dates">${from}&nbsp;&mdash;&nbsp;${to}</p>`;
};

const createSiteInfoMenuTemplate = (pointsInfo) => {
  const {cost, dateFrom, dateTo, cities} = pointsInfo || {};

  return (
    `<section class="trip-main__trip-info  trip-info">
      <div class="trip-info__main">
        ${cities ? createCitiesTemplate(cities) : ``}

        ${dateFrom && dateTo ? createDateTemplate(dateFrom, dateTo) : ``}
      </div>

      <p class="trip-info__cost">
        Total: &euro;&nbsp;<span class="trip-info__cost-value">${cost ? cost : 0}</span>
      </p>
    </section>`
  );
};

export default class SiteInfoMenu extends AbstractComponent {
  constructor(pointsInfo) {
    super();

    this._pointsInfo = pointsInfo;
  }

  getTemplate() {
    return createSiteInfoMenuTemplate(this._pointsInfo);
  }
}
