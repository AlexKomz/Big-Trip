import AbstractSmartComponent from "./abstract-smart-component";
import {SortType} from "../consts";


const createSortTemplate = (currentSortType, time, price) => {
  return (
    `<form class="trip-events__trip-sort  trip-sort" action="#" method="get">
      <span class="trip-sort__item  trip-sort__item--day">Day</span>

      <div class="trip-sort__item  trip-sort__item--event">
        <input
          id="sort-event"
          class="trip-sort__input  visually-hidden"
          type="radio"
          name="trip-sort"
          value="sort-event"
          ${currentSortType === SortType.DEFAULT ? `checked` : ``}>
        <label class="trip-sort__btn" for="sort-event" data-sort-type=${SortType.DEFAULT}>Event</label>
      </div>

      <div class="trip-sort__item  trip-sort__item--time">
        <input
          id="sort-time"
          class="trip-sort__input  visually-hidden"
          type="radio"
          name="trip-sort"
          value="sort-time"
          ${(currentSortType === SortType.TIME_UP || currentSortType === SortType.TIME_DOWN) ? `checked` : ``}>
        <label class="trip-sort__btn" for="sort-time" data-sort-type=${time}>Time</label>
      </div>

      <div class="trip-sort__item  trip-sort__item--price">
        <input
          id="sort-price"
          class="trip-sort__input  visually-hidden"
          type="radio"
          name="trip-sort"
          value="sort-price"
          ${(currentSortType === SortType.PRICE_UP || currentSortType === SortType.PRICE_DOWN) ? `checked` : ``}>
        <label class="trip-sort__btn" for="sort-price" data-sort-type=${price}>Price</label>
      </div>

      <span class="trip-sort__item  trip-sort__item--offers">Offers</span>
    </form>`
  );
};

export default class Sort extends AbstractSmartComponent {
  constructor() {
    super();

    this._time = `time-up`;
    this._price = `price-up`;

    this._currentSortType = SortType.DEFAULT;

    this._sortTypeChangeHandler = null;
  }

  sortDefaultStates() {
    this._time = `time-up`;
    this._price = `price-up`;
  }

  getTemplate() {
    return createSortTemplate(this._currentSortType, this._time, this._price);
  }

  getSortType() {
    return this._currentSortType;
  }

  setSortType(newSortType) {
    this._currentSortType = newSortType;
    this.sortDefaultStates();
    this.rerender();
  }

  recoveryListeners() {
    this.setSortTypeChangeHandler(this._sortTypeChangeHandler);
  }

  rerender() {
    super.rerender();
  }

  setSortTypeChangeHandler(handler) {
    this._sortTypeChangeHandler = handler;

    this.getElement().addEventListener(`click`, (evt) => {
      evt.preventDefault();

      if (evt.target.tagName !== `LABEL`) {
        return;
      }

      const sortType = evt.target.dataset.sortType;

      this._currentSortType = sortType;

      this._sortTypeChangeHandler(this._currentSortType);

      const splitedSortType = sortType.split(`-`);
      const newSortType = splitedSortType[0] + `-` + (splitedSortType[1] === `down` ? `up` : `down`);

      switch (sortType) {
        case this._time:
          this._time = newSortType;
          break;
        case this._price:
          this._price = newSortType;
          break;
      }

      if (sortType === SortType.DEFAULT) {
        this.sortDefaultStates();
      }

      this.rerender();
    });
  }
}
