import AbstractComponent from "./abstract-component";
import {hoursMinutsFromDate, getDatetimeString} from "../utils/common";

const createOffersTemplate = (offers) => {
  return offers.map((offer) => offer.checked ? (
    `<li class="event__offer">
      <span class="event__offer-title">${offer.title}</span>
      &plus;
      &euro;&nbsp;<span class="event__offer-price">${offer.price}</span>
    </li>`
  ) : ``).join(``);
};

const createPointTemplate = (point) => {
  const {
    type,
    destination,
    beginDate,
    endDate,
    duration,
    basePrice,
    offers
  } = point;

  const pretext = [`check-in`, `sightseeing`, `restaurant`].includes(type)
    ? `in`
    : `to`;

  const offersTemplate = createOffersTemplate(offers);

  const capitalizedType = type[0].toUpperCase() + type.slice(1);

  return (
    `<li class="trip-events__item">
      <div class="event">
        <div class="event__type">
          <img class="event__type-icon" width="42" height="42" src="img/icons/${type.toLowerCase()}.png" alt="Event type icon">
        </div>
        <h3 class="event__title">${capitalizedType} ${pretext} ${destination.name}</h3>

        <div class="event__schedule">
          <p class="event__time">
            <time class="event__start-time" datetime="${getDatetimeString(beginDate, true)}">${hoursMinutsFromDate(beginDate)}</time>
            &mdash;
            <time class="event__end-time" datetime="${getDatetimeString(endDate, true)}">${hoursMinutsFromDate(endDate)}</time>
          </p>
          <p class="event__duration">${`${duration.days > 0 ? duration.days + `D` : ``} ${duration.hours > 0 ? duration.hours + `H` : ``} ${duration.minutes}M`}</p>
        </div>

        <p class="event__price">
          &euro;&nbsp;<span class="event__price-value">${basePrice}</span>
        </p>

        <h4 class="visually-hidden">Offers:</h4>
        <ul class="event__selected-offers">
          ${offersTemplate}
        </ul>

        <button class="event__rollup-btn" type="button">
          <span class="visually-hidden">Open event</span>
        </button>
      </div>
    </li>`
  );
};

export default class Point extends AbstractComponent {
  constructor(point) {
    super();

    this._point = point;
  }

  getTemplate() {
    return createPointTemplate(this._point);
  }

  setEditClickHandler(handler) {
    this.getElement().querySelector(`.event__rollup-btn`).addEventListener(`click`, handler);
  }
}
