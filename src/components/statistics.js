import Chart from "chart.js";
import ChartDataLabels from 'chartjs-plugin-datalabels';
import AbstractSmartComponent from "./abstract-smart-component";


const BAR_HEIGHT = 55;

const renderMoneyChart = (moneyCtx, points) => {
  const labels = new Set();
  points.forEach((point) => labels.add(point.type));

  const money = [];

  for (const label of labels) {
    const result = points.reduce((sum, point) => point.type === label ? sum + +point.basePrice : sum, 0);

    money.push({
      label,
      result
    });
  }

  money.sort((moneyA, moneyB) => moneyB.result - moneyA.result);

  return new Chart(moneyCtx, {
    plugins: [ChartDataLabels], type: `horizontalBar`,
    data: {
      labels: money.map((it) => it.label.toUpperCase()),
      datasets: [{
        data: money.map((it) => it.result),
        backgroundColor: `#ffffff`,
        hoverBackgroundColor: `#ffffff`,
        anchor: `start`,
        barThickness: 44,
        minBarLength: 50
      }]
    },
    options: {
      plugins: {
        datalabels: {
          font: {
            size: 13
          },
          color: `#000000`,
          anchor: `end`,
          align: `start`,
          formatter: (val) => `€ ${val}`
        }
      },
      title: {
        display: true,
        text: `MONEY`,
        fontColor: `#000000`,
        fontSize: 23,
        position: `left`
      },
      scales: {
        yAxes: [{
          ticks: {
            fontColor: `#000000`, padding: 5,
            fontSize: 13,
          },
          gridLines: {
            display: false,
            drawBorder: false
          },
        }],
        xAxes: [{
          ticks: {
            display: false,
            beginAtZero: true,
          },
          gridLines: {
            display: false,
            drawBorder: false
          },
        }],
      },
      legend: {
        display: false
      },
      tooltips: {
        enabled: false,
      }
    }
  });
};

const renderTransportChart = (transportCtx, points) => {
  const labels = {
    'FLY': [`flight`],
    'DRIVE': [`drive`],
    'RIDE': [`taxi`, `bus`, `train`],
    'SAIL': [`ship`]
  };
  const transport = [];

  for (const [label, selector] of Object.entries(labels)) {
    const result = points.reduce((count, point) => selector.includes(point.type.toLowerCase()) ? count + 1 : count, 0);
    transport.push({
      label,
      result
    });
  }

  transport.sort((transportA, transportB) => transportB.result - transportA.result);

  return new Chart(transportCtx, {
    plugins: [ChartDataLabels],
    type: `horizontalBar`,
    data: {
      labels: transport.map((it) => it.label),
      datasets: [{
        data: transport.map((it) => it.result),
        backgroundColor: `#ffffff`,
        hoverBackgroundColor: `#ffffff`,
        anchor: `start`,
        barThickness: 44,
        minBarLength: 50
      }]
    },
    options: {
      plugins: {
        datalabels: {
          font: {
            size: 13
          },
          color: `#000000`,
          anchor: `end`,
          align: `start`,
          formatter: (val) => `${val}x`
        }
      },
      title: {
        display: true,
        text: `TRANSPORT`,
        fontColor: `#000000`,
        fontSize: 23,
        position: `left`
      },
      scales: {
        yAxes: [{
          ticks: {
            fontColor: `#000000`,
            padding: 5,
            fontSize: 13,
          }, gridLines: {
            display: false,
            drawBorder: false
          },
        }],
        xAxes: [{
          ticks: {
            display: false,
            beginAtZero: true,
          },
          gridLines: {
            display: false,
            drawBorder: false
          },
        }],
      },
      legend: {
        display: false
      },
      tooltips: {
        enabled: false,
      }
    }
  });
};

const convertToMinutes = (duration) => {
  let hours = 0;

  if (duration.days > 0) {
    hours += duration.days * 24;
  }

  if (duration.hours > 0) {
    hours += duration.hours;
  }

  return hours + Math.ceil(duration.minutes / 60);
};

const renderTimeSpendChart = (timeSpendCtx, points) => {
  const labels = new Set();
  points.forEach((point) => labels.add(point.destination));

  const timeSpend = [];

  let startObj = {
    days: 0,
    hours: 0,
    minutes: 0
  };

  for (const label of labels) {
    const duration = points.reduce((sum, point) => point.destination === label
      ? {
        days: sum.days + point.duration.days,
        hours: sum.hours + point.duration.hours,
        minutes: sum.minutes + point.duration.minutes
      } : sum, startObj);

    const result = convertToMinutes(duration);

    timeSpend.push({
      label,
      result
    });
  }

  timeSpend.sort((timeSpendA, timeSpendB) => timeSpendB.result - timeSpendA.result);

  return new Chart(timeSpendCtx, {
    plugins: [ChartDataLabels],
    type: `horizontalBar`,
    data: {
      labels: timeSpend.map((it) => it.label.name.toUpperCase()),
      datasets: [{
        data: timeSpend.map((it) => it.result),
        backgroundColor: `#ffffff`,
        hoverBackgroundColor: `#ffffff`,
        anchor: `start`,
        barThickness: 44,
        minBarLength: 50
      }]
    },
    options: {
      plugins: {
        datalabels: {
          font: {
            size: 13
          },
          color: `#000000`,
          anchor: `end`,
          align: `start`,
          formatter: (val) => `${val}H`
        }
      },
      title: {
        display: true,
        text: `TIME SPEND`,
        fontColor: `#000000`,
        fontSize: 23,
        position: `left`
      },
      scales: {
        yAxes: [{
          ticks: {
            fontColor: `#000000`,
            padding: 5,
            fontSize: 13,
          }, gridLines: {
            display: false,
            drawBorder: false
          },
        }],
        xAxes: [{
          ticks: {
            display: false,
            beginAtZero: true,
          },
          gridLines: {
            display: false,
            drawBorder: false
          },
        }],
      },
      legend: {
        display: false
      },
      tooltips: {
        enabled: false,
      }
    }
  });
};

const createStatisticsTemptale = () => {
  return (
    `<section class="statistics">
      <h2 class="visually-hidden">Trip statistics</h2>

      <div class="statistics__item statistics__item--money">
        <canvas class="statistics__chart  statistics__chart--money"
                width="900"></canvas>
      </div>

      <div class="statistics__item statistics__item--transport">
        <canvas class="statistics__chart  statistics__chart--transport"
                width="900"></canvas>
      </div>

      <div class="statistics__item statistics__item--time-spend">
        <canvas class="statistics__chart  statistics__chart--time"
                width="900"></canvas>
      </div>
    </section>`
  );
};

export default class Statistics extends AbstractSmartComponent {
  constructor(pointsModel) {
    super();

    this._pointsModel = pointsModel;

    this._moneyChart = null;
    this._transportChart = null;
    this._timeSpendChart = null;

    this._onFilterChange = this._onFilterChange.bind(this);

    this._pointsModel.setFilterChangeHandler(this._onFilterChange);

    this._renderCharts();
  }

  getTemplate() {
    return createStatisticsTemptale();
  }

  show() {
    super.show();

    this.rerender();
  }

  recoveryListeners() { }

  rerender() {
    super.rerender();

    this._renderCharts();
  }

  _onFilterChange() {
    this._renderCharts();
  }

  _renderCharts() {
    const element = this.getElement();

    const moneyCtx = element.querySelector(`.statistics__chart--money`);
    const transportCtx = element.querySelector(`.statistics__chart--transport`);
    const timeSpendCtx = element.querySelector(`.statistics__chart--time`);

    moneyCtx.height = BAR_HEIGHT * 6;
    transportCtx.height = BAR_HEIGHT * 4;
    timeSpendCtx.height = BAR_HEIGHT * 4;

    const points = this._pointsModel.Points;

    this._resetCharts();

    this._moneyChart = renderMoneyChart(moneyCtx, points);
    this._transportChart = renderTransportChart(transportCtx, points);
    this._timeSpendChart = renderTimeSpendChart(timeSpendCtx, points);
  }

  _resetCharts() {
    if (this._moneyChart) {
      this._moneyChart.destroy();
      this._moneyChart = null;
    }

    if (this._transportChart) {
      this._transportChart.destroy();
      this._transportChart = null;
    }

    if (this._timeSpendChart) {
      this._timeSpendChart.destroy();
      this._timeSpendChart = null;
    }
  }
}
