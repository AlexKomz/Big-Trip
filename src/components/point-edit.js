import AbstractSmartComponent from "./abstract-smart-component";

import flatpickr from "flatpickr";

import "flatpickr/dist/flatpickr.min.css";


const DefaultData = {
  deleteButtonText: `Delete`,
  saveButtonText: `Save`,
};

const generateTypeGroup = (type, array, legend) => {
  let htmlString = (
    `<fieldset class="event__type-group">
      <legend class="visually-hidden">${legend}</legend>`
  );

  for (const item of array) {
    htmlString += (
      `<div class="event__type-item">
        <input id="event-type-${item.toLowerCase()}-1" class="event__type-input  visually-hidden" type="radio" name="event-type" value="${item.toLowerCase()}"
        ${item === type ? `checked` : ``}>
        <label class="event__type-label  event__type-label--${item.toLowerCase()}" for="event-type-${item.toLowerCase()}-1">${item}</label>
      </div>`
    );
  }

  htmlString += (
    `</fieldset>`
  );

  return htmlString;
};

const generateCity = (city) => {
  return `<option value="${city}"></option>`;
};

const generateFavoriteCheckbox = (isFavorite) => {
  return (
    `<input id="event-favorite-1" class="event__favorite-checkbox  visually-hidden" type="checkbox" name="event-favorite" ${isFavorite ? `checked` : ``}>
    <label class="event__favorite-btn" for="event-favorite-1">
      <span class="visually-hidden">Add to favorite</span>
      <svg class="event__favorite-icon" width="28" height="28" viewBox="0 0 28 28">
        <path d="M14 21l-8.22899 4.3262 1.57159-9.1631L.685209 9.67376 9.8855 8.33688 14 0l4.1145 8.33688 9.2003 1.33688-6.6574 6.48934 1.5716 9.1631L14 21z"/>
      </svg>
    </label>`
  );
};

const generateOffer = (offer) => {
  return (
    `<div class="event__offer-selector">
      <input class="event__offer-checkbox  visually-hidden" id="event-offer-${offer.title.toLowerCase()}-1"
      type="checkbox"
      name="event-offer-${offer.title.toLowerCase()}" ${offer.checked ? `checked` : ``}>
      <label class="event__offer-label" for="event-offer-${offer.title.toLowerCase()}-1">
        <span class="event__offer-title">Add ${offer.title}</span>
        &plus;
        &euro;&nbsp;<span class="event__offer-price">${offer.price}</span>
      </label>
    </div>`
  );
};

const createOffersTemplate = (offers) => {
  return (
    `<section class="event__section  event__section--offers">
      <h3 class="event__section-title  event__section-title--offers">Offers</h3>
      <div class="event__available-offers">
        ${offers.map((offer) => generateOffer(offer)).join(`\n`)}
      </div>
    </section>`
  );
};

const createPointEditTemplate = (point, cities, options = {}) => {
  const {
    beginDate,
    endDate,
    basePrice
  } = point;

  const {type, destinationName, isFavorite, offers, externalData} = options;

  const pretext = [`check-in`, `sightseeing`, `restaurant`].includes(type)
    ? `in`
    : `to`;

  const transfers = [
    `Taxi`,
    `Bus`,
    `Train`,
    `Ship`,
    `Transport`,
    `Drive`,
    `Flight`
  ];

  const activities = [
    `Check-in`,
    `Sightseeing`,
    `Restaurant`
  ];

  const offersTemplate = offers.length > 0 ? createOffersTemplate(offers) : ``;

  const capitalizedType = type[0].toUpperCase() + type.slice(1);

  const deleteButtonText = externalData.deleteButtonText;
  const saveButtonText = externalData.saveButtonText;

  return (
    `<li class="trip-events__item">
      <form class="trip-events__item  event  event--edit" action="#" method="post">
        <header class="event__header">
          <div class="event__type-wrapper">
            <label class="event__type  event__type-btn" for="event-type-toggle-1">
              <span class="visually-hidden">Choose event type</span>
              <img class="event__type-icon" width="17" height="17" src="img/icons/${type.toLowerCase()}.png" alt="Event type icon">
            </label>
            <input class="event__type-toggle  visually-hidden" id="event-type-toggle-1" type="checkbox">

            <div class="event__type-list">
              ${generateTypeGroup(capitalizedType, transfers, `Transfer`)}
              ${generateTypeGroup(capitalizedType, activities, `Activity`)}
            </div>
          </div>

          <div class="event__field-group  event__field-group--destination">
            <label class="event__label  event__type-output" for="event-destination-1">
              ${capitalizedType} ${pretext}
            </label>
            <input class="event__input  event__input--destination" id="event-destination-1" type="text" name="event-destination" value="${destinationName}" list="destination-list-1">
            <datalist id="destination-list-1">
              ${cities.map((city) => generateCity(city)).join(`\n`)}
            </datalist>
          </div>

          <div class="event__field-group  event__field-group--time">
            <label class="visually-hidden" for="event-start-time-1">
              From
            </label>
            <input class="event__input  event__input--time" id="event-start-time-1" type="text" name="event-start-time" value="${beginDate}">
            &mdash;
            <label class="visually-hidden" for="event-end-time-1">
              To
            </label>
            <input class="event__input  event__input--time" id="event-end-time-1" type="text" name="event-end-time" value="${endDate}">
          </div>

          <div class="event__field-group  event__field-group--price">
            <label class="event__label" for="event-price-1">
              <span class="visually-hidden">Price</span>
              &euro;
            </label>
            <input class="event__input  event__input--price" id="event-price-1" type="text" name="event-price" value="${basePrice}">
          </div>

          <button class="event__save-btn  btn  btn--blue" type="submit">${saveButtonText}</button>
          <button class="event__reset-btn" type="reset">${deleteButtonText}</button>

          ${generateFavoriteCheckbox(isFavorite)}

          <button class="event__rollup-btn" type="button">
            <span class="visually-hidden">Open event</span>
          </button>
        </header>
        <section class="event__details">
          ${offersTemplate}
        </section>
      </form>
    </li>`
  );
};

export default class PointEdit extends AbstractSmartComponent {
  constructor(point, destinationsModel, offersModel) {
    super();

    this._point = point;
    this._destinationsModel = destinationsModel;
    this._offersModel = offersModel;

    this._type = point.type;
    this._destinationName = point.destination.name;
    this._details = {
      description: point.destination.description,
      pictures: point.destination.pictures
    };
    this._isFavoritePoint = point.isFavorite;
    this._pointOffers = point.offers.map((offer) => Object.assign({}, offer));

    this._externalData = DefaultData;

    this._flatpickrBegin = null;

    this._submitHandler = null;
    this._closeHandler = null;
    this._typeChangeHandler = null;
    this._deleteButtonClickHandler = null;

    this._applyFlatpickrBegin();
    this._applyFlatpickrEnd();
    this._subscribeOnEvents();
  }

  getTemplate() {
    return createPointEditTemplate(this._point, this._destinationsModel.Cities, {
      type: this._type,
      destinationName: this._destinationName,
      isFavorite: this._isFavoritePoint,
      offers: this._pointOffers,
      externalData: this._externalData,
    });
  }

  removeElement() {
    if (this._flatpickrBegin) {
      this._flatpickrBegin.destroy();
      this._flatpickrBegin = null;
    }

    if (this._flatpickrEnd) {
      this._flatpickrEnd.destroy();
      this._flatpickrEnd = null;
    }

    super.removeElement();
  }

  recoveryListeners() {
    this.setFormSubmitHandler(this._submitHandler);
    this.setCloseClickHandler(this._closeHandler);
    this.setDeleteButtonClickHandler(this._deleteButtonClickHandler);
    this._subscribeOnEvents();
  }

  rerender() {
    super.rerender();

    this._applyFlatpickrBegin();
    this._applyFlatpickrEnd();
  }

  reset() {
    const point = this._point;

    this._type = point.type;
    this._destinationName = point.destination.name;
    this._details = {
      description: point.description,
      pictures: point.photos
    };
    this._isFavoritePoint = point.isFavorite;
    this._pointOffers = point.offers.map((offer) => Object.assign({}, offer));

    this.rerender();
  }

  get formData() {
    const form = this.getElement().querySelector(`.event--edit`);

    return new FormData(form);
  }

  get Data() {
    return Object.assign({}, {
      "type": this._type,
      "id": this._point.id,
      "is_favorite": this._isFavoritePoint,
      "offers": this._pointOffers,
      "destination": Object.assign({name: this._destinationName}, this._details)
    });
  }

  set Data(data) {
    this._externalData = Object.assign({}, DefaultData, data);
    this.rerender();
  }

  setFormSubmitHandler(handler) {
    this.getElement().querySelector(`form`).addEventListener(`submit`, handler);
    this._submitHandler = handler;
  }

  setCloseClickHandler(handler) {
    this.getElement().querySelector(`.event__rollup-btn`).addEventListener(`click`, handler);
    this._closeHandler = handler;
  }

  setDeleteButtonClickHandler(handler) {
    this.getElement().querySelector(`.event__reset-btn`).addEventListener(`click`, handler);
    this._deleteButtonClickHandler = handler;
  }

  _applyFlatpickrBegin() {
    if (this._flatpickrBegin) {
      this._flatpickrBegin.destroy();
      this._flatpickrBegin = null;
    }

    const beginDateInput = this.getElement().querySelector(`#event-start-time-1`);
    this._flatpickrBegin = flatpickr(beginDateInput, {
      allowInput: true,
      defaultDate: this._point.beginDate || `today`,
      enableTime: true,
      dateFormat: `d/m/y H:i`,
    });
  }

  _applyFlatpickrEnd() {
    if (this._flatpickrEnd) {
      this._flatpickrEnd.destroy();
      this._flatpickrEnd = null;
    }

    const endDateInput = this.getElement().querySelector(`#event-end-time-1`);
    this._flatpickrEnd = flatpickr(endDateInput, {
      allowInput: true,
      defaultDate: this._point.endDate || `today`,
      enableTime: true,
      dateFormat: `d/m/y H:i`,
    });
  }

  _subscribeOnEvents() {
    const element = this.getElement();

    element.querySelector(`.event__type-list`).addEventListener(`click`, (evt) => {
      if (evt.target.tagName !== `LABEL`) {
        return;
      }

      this._type = evt.target.innerText.toLowerCase();
      this._pointOffers = this._offersModel.getOffers(this._type);

      this.rerender();
    });

    element.querySelector(`.event__input--destination`).addEventListener(`change`, (evt) => {
      this._destinationName = evt.target.value;

      this._details = this._destinationsModel.getDetails(this._destinationName);

      this.rerender();
    });

    element.querySelector(`.event__favorite-btn`).addEventListener(`click`, () => {
      this._isFavoritePoint = !this._isFavoritePoint;
    });

    const availibleOffers = element.querySelector(`.event__available-offers`);

    if (availibleOffers) {
      availibleOffers.addEventListener(`click`, (evt) => {
        const label = evt.target.closest(`label`);

        if (!label) {
          return;
        }

        if (!availibleOffers.contains(label)) {
          return;
        }

        const input = label.previousElementSibling;

        for (let offer of this._pointOffers) {
          if (input.name.includes(offer.title.toLowerCase())) {
            offer.checked = !offer.checked;
            break;
          }
        }
      });
    }
  }
}
